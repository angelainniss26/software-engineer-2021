import React from "react";

import "./table-cell.scss";

type Props = {
    children: any,

};

const TableCell = ({children}:Props) => {
  const baseClass = "c-table-cell";

  return <td  className={baseClass}>{children}</td>;
};

export default TableCell;
