import React from "react";

import "./table.scss";

type Props = {
  children: any,
};

const Table = ({ children }: Props) => {
  const baseClass = "c-table";

  return (
    <table role="table" className={baseClass}>
      {children}
    </table>
  );
};

export default Table;
