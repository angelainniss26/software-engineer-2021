import React from "react";

import "./tablerow.scss";

type Props = {
  children: any,
  id: number,
};

const TableRow = ({ children, id }: Props) => {
  const baseClass = "c-table-row";

  return (
    <tr id={id} className={baseClass}>
      {children}
    </tr>
  );
};

export default TableRow;
