// @flow
import React, { useState } from "react";

import InputField from "../InputField/InputField";
import Button from "../Button/Button";

import validate from "../../helperFunctions/validations";

import { v4 as uuidv4 } from "uuid";

import "./inputform.scss";

type Props = {
  sendData: (string) => {},
};

const InputForm = ({ sendData }: Props) => {
  const baseClass = "c-input-form";

  const [name, setName] = useState("");
  const [nameError, setNameError] = useState("");
  const [email, setEmail] = useState("");
  const [emailError, setEmailError] = useState("");

  const onNameInputChange = (event) => {
    setName(event.target.value);
  };

  const onEmailInputChange = (event) => {
    setEmail(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    const isFormValid = validate(name, email, setNameError, setEmailError);
    if (!isFormValid) {
      return;
    }
    sendData({ name, email, id: uuidv4() });
    setName("");
    setEmail("");
    setNameError("");
    setEmailError("");
  };

  return (
    <div className={`${baseClass}__form-container`}>
      <form className={baseClass} htmlFor="input-form">
        <label htmlFor="name">Name</label>
        <InputField
          aria-required="true"
          type="text"
          placeholder="ex. Jane Doe"
          name="name"
          onChange={onNameInputChange}
          id="name"
          value={name}
          error={nameError}
          ariaLabelledBy="name"
        />
        <label htmlFor="email">Email</label>
        <InputField
          aria-required="true"
          type="email"
          placeholder="ex. Jane Doe@gmail.com"
          name="email"
          onChange={onEmailInputChange}
          id="email"
          value={email}
          error={emailError}
          ariaLabelledBy="email"
        />
        <Button
          className={`${baseClass}__add-btn`}
          label="Add"
          type="submit"
          onClick={handleSubmit}
        />
      </form>
    </div>
  );
};

export default InputForm;
