// @flow
import React from "react";

import "./button.scss";

type Props = {
  label: string,
  onClick: ({}) => void,
  disabled: boolean,
};

const Button = ({ label, onClick, disabled }: Props) => {
  const baseClass = "c-button";

  return (
    <button
      type="button"
      className={baseClass}
      disabled={disabled}
      onClick={onClick}
    >
      {label}
    </button>
  );
};

Button.defaultProps = {
  label: "Button",
  disabled: false,
};

export default Button;
