import React from "react";

type Props = {
  children: any,
};

const TableBody = ({ children }: Props) => {
  return <tbody>{children}</tbody>;
};

export default TableBody;
