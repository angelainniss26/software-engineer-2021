// @flow

import React from "react";

import "./inputField.scss";

type Props = {
  id: string,
  placeholder: string,
  label: string,
  disabled: boolean,
  value: string,
  onChange: (string) => void,
  type: "text" | "number" | "password" | "date" | "time",
  error: string,
  ariaLabelledBy: string
};
const InputField = ({
  id,
  placeholder,
  label,
  disabled,
  onChange,
  type,
  value,
  name,
  error,
  ariaLabelledBy,
}: Props) => {
  const baseClass = "input-field";
  return (
    <>
      <div className={baseClass}>
        <input
          id={id}
          label={label}
          placeholder={placeholder}
          value={value}
          onChange={onChange}
          disabled={disabled}
          type={type}
          name={name}
          arialabelledby={ariaLabelledBy}
        />
        <div className={`${baseClass}__error`}> {error}</div>
      </div>
    </>
  );
};

InputField.defaultProps = {
  label: null,
  error: "",
  placeholder: "",
  disabled: false,
  value: "",
  type: "text",
  name: "",
};
export default InputField;
