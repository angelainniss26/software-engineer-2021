const validate = (name, email, setNameError, setEmailError) => {
  let formIsValid = true;
  if (!name.trim().length) {
    formIsValid = false;
    setNameError("Please enter a name");
  }
  let letters = /[0-9!?,;*]/;
  if (letters.test(name)) {
    formIsValid = false;
    setNameError("Please only enter letters, no special characters or numbers");
  }
  if (!email.trim().length) {
    formIsValid = false;
    setEmailError("Please enter an email address");
  }

  let includeAt = /[^@]+@[^.]+\.\w+/;
  if (!includeAt.test(email)) {
    formIsValid = false;
    setEmailError("Please enter a valid email address");
  }

  return formIsValid;
};
export default validate;
