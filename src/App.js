import React, { useState } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";

import InputForm from "./Components/Form/InputForm";
import Table from "./Components/Table/Table";
import TableBody from "./Components/TableBody/tableBody";
import TableRow from "./Components/TableRow/TableRow";
import TableCell from "./Components/TableCell/TableCell";
import Button from "./Components/Button/Button";

import capitalizeFirstLetter from "../src/helperFunctions/capitalize";
import compare from "./helperFunctions/compare";

import "./app.scss";

const App = () => {
  const baseClass = "c-app";

  const [userList, setUserList] = useState([
    { name: "Angela", email: "Angela@gmail.com", id: 1 },
    { name: "Jessica", email: "Jess@hotmail.co.uk", id: 2 },
    { name: "Joshua", email: "Joshie@hotmail.com", id: 3 },
    { name: "Aaron", email: "Aaron@gmail.com", id: 4 },
  ]);

  const handleUserInputData = (userData) => {
    const capitalizedName = capitalizeFirstLetter(userData.name);
    userData.name = capitalizedName;
    setUserList([...userList, userData]);
  };

  const onDeleteRow = (selectedUser) => {
    alert(`Are you sure you want to delete ${selectedUser.name}?`);
    setUserList([
      ...userList.filter((user) => {
        if (user.id !== selectedUser.id) {
          return user.id;
        }
      }),
    ]);
  };

  const sortedUserList = userList.sort(compare);

  const userIcon = (
    <FontAwesomeIcon style={{ color: "#3cb3c9", opacity: 0.5 }} icon={faUser} />
  );

  return (
    <div className={baseClass}>
      <h1>HR Employee List - Apolitical</h1>
      <InputForm sendData={handleUserInputData} id={1} />
      <div className={`${baseClass}__table-wrapper`}>
        <Table>
          <caption
            style={{ textAlign: "left", fontSize: "12px", fontStyle: "italic" }}
          >
            Table containing employee names and emails
          </caption>
          <TableBody>
            <TableRow>
              <th>{null}</th>
              <th>Employee name</th>
              <th>Employee email</th>
              <th>Delete user?</th>
            </TableRow>
            {sortedUserList.map((user, index) => {
              return (
                <>
                  <TableRow id={index} key={index}>
                    <TableCell>{userIcon}</TableCell>
                    <TableCell>{user.name}</TableCell>
                    <TableCell>{user.email}</TableCell>
                    <TableCell>
                      <Button
                        label="Delete"
                        onClick={() => onDeleteRow(user)}
                      />
                    </TableCell>
                  </TableRow>
                </>
              );
            })}
          </TableBody>
        </Table>
      </div>
    </div>
  );
};

export default App;
